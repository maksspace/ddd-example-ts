import { Entity } from './entity';
import { User } from './user';

export class Message extends Entity {
    constructor (id: string, private creator: User, private text: string) {
        super(id);
    }

    getCreator () {
        return this.creator;
    }

    getText () {
        return this.text;
    }
}