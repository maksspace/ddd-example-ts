import { Entity } from './entity';
import { User } from './user';
import { Message } from './message';

export class Chat extends Entity {
    private users: User[];
    private messages: Message[];

    constructor (id: string) {
        super(id);
        this.users = [];
        this.messages = [];
    }

    getUsers () {
        return this.users;
    }

    getMessages () {
        return this.messages;
    }

    join (user: User) {
        if (user.getStatus() !== 'activated') {
            throw new Error('Cant join for not activated users');
        }
        if (this.isJoined(user.getId())) {
            throw new Error('Already joined');
        }
        this.users.push(user);
    }

    sendMessage (message: Message) {
        if (!this.isJoined(message.getCreator().getId())) {
            throw new Error('User is not joined');
        }
        this.messages.push(message);
    }

    removeMessage (id: string) {
        this.messages = this.messages.filter((m: Message) => m.getId() !== id);
    }

    private isJoined (userId: string) {
        const joined = this.users.find(u => u.getId() === userId);
        return !!joined;
    }
}
