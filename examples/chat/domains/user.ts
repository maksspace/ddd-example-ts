import { Entity } from './entity'

export class User extends Entity {
    private name: string;
    private status: string;
    constructor (id: string, name: string, status: string) {
        super(id);
        this.name = name;
        this.status = status;
    }

    changeStatus (status: string) {
        this.status = status;
    }

    getStatus (): string {
        return this.status;
    }

    getName () {
        return this.name;
    }
}