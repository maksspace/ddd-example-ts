import * as uuid from 'uuid';
import { Chat } from '../domains/chat';
import { Message } from '../domains/message';
import { ChatRepository } from '../repositories/chat';
import { UserRepository } from '../repositories/user';
import { MessageRepository } from '../repositories/message';

export class ChatService {

    private userRepo: UserRepository;
    private chatRepo: ChatRepository;
    private messageRepo: MessageRepository;

    constructor () {
        this.chatRepo = new ChatRepository();
        this.userRepo = new UserRepository();
        this.messageRepo = new MessageRepository();
    }

    createChat(): string {
        const chat = new Chat(uuid.v1());
        return this.chatRepo.save(chat);
    }

    getChat (id: string): Chat {
        return this.chatRepo.find(id);
    }

    join (chatId: string, userId: string) {
        const user = this.userRepo.find(userId);
        const chat = this.chatRepo.find(chatId);
        chat.join(user);
        this.chatRepo.save(chat);
    }

    sendMessage (chatId: string, userId: string, text: string) {
        const user = this.userRepo.find(userId);
        const chat = this.chatRepo.find(chatId);
        const message = new Message(uuid.v1(), user, text);
        chat.sendMessage(message);
        this.messageRepo.save(message);
        this.chatRepo.save(chat);
    }

}