import * as uuid from 'uuid';
import { User } from '../domains/user';
import { UserRepository } from '../repositories/user';

export class UserService {

    private userRepo: UserRepository;

    constructor () {
        this.userRepo = new UserRepository();
    }

    create(name: string): string {
        const id = uuid.v1();
        const user = new User(id, name, 'activated');
        return this.userRepo.save(user);
    }

}