import * as fs from 'fs';

import { Repository } from './interfaces';
import { Message } from '../domains/message';
import { UserRepository } from './user';

export class MessageRepository implements Repository {
    private userRepo: UserRepository;
    constructor () {
        this.userRepo = new UserRepository();
    }

    find(id: string): Message {
        const fData = JSON.parse(fs.readFileSync('./message.json').toString());
        const mData = fData[id];
        if (!mData) {
            throw new Error('Message not found');
        }
        const user = this.userRepo.find(mData.creatorId);
        return new Message(id, user, mData.text)
    }

    findMany() {}

    save (message: Message): string {
        const fData = JSON.parse(fs.readFileSync('./message.json').toString());
        fData[message.getId()] = {
            creatorId: message.getCreator().getId(),
            text: message.getText()
        };
        fs.writeFileSync('./message.json', JSON.stringify(fData, null, 4));
        return message.getId();
    }

    update() {}

    remove () {}

}