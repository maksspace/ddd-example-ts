import * as fs from 'fs';

import { Repository } from './interfaces';
import { User } from '../domains/user';


export class UserRepository implements Repository {

    find(id: string): User {
        const fData = JSON.parse(fs.readFileSync('./user.json').toString());
        const uData = fData[id];
        if (!uData) {
            throw new Error('User not found');
        }
        return new User(id, uData.name, uData.status);
    }

    findMany() {}

    save (user: User): string {
        const fData = JSON.parse(fs.readFileSync('./user.json').toString());
        fData[user.getId()] = {
            name: user.getName(),
            status: user.getStatus()
        };
        fs.writeFileSync('./user.json', JSON.stringify(fData, null, 4));
        return user.getId();
    }

    update() {}

    remove () {}

}