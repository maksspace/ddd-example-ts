import { MessageRepository } from './message';
import { UserRepository } from './user';
import * as fs from 'fs';

import { Repository } from './interfaces';
import { Chat } from '../domains/chat';
import { Message } from '../domains/message';
import { User } from '../domains/user';


export class ChatRepository implements Repository {
    private userRepo: UserRepository;
    private messageRepo: MessageRepository;

    constructor () {
        this.userRepo = new UserRepository;
        this.messageRepo = new MessageRepository;
    }

    find(id: string): Chat {
        const fData = JSON.parse(fs.readFileSync('./chat.json').toString());
        const chatData = fData[id];
        if (!chatData) {
            throw new Error('Chat not found');
        }
        const chat = new Chat(id);
        if (chatData.users) {
            for (const userId of (chatData.users as string[])) {
                const user = this.userRepo.find(userId);
                chat.join(user);
            }
        }
        if (chatData.messages) {
            for (const messageId of (chatData.messages as any[])) {
                const message = this.messageRepo.find(messageId);
                chat.sendMessage(message);
            }
        }
        return chat;
    }

    findMany() {}

    save (chat: Chat): string {
        const fData = JSON.parse(fs.readFileSync('./chat.json').toString());
        fData[chat.getId()] = {
            users: chat.getUsers().map(u => u.getId()),
            messages: chat.getMessages().map(m => m.getId())
        };
        fs.writeFileSync('./chat.json', JSON.stringify(fData, null, 4));
        return chat.getId();
    }

    update() {}

    remove () {}

}