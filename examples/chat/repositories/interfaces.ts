export interface Repository {
    find(id: string): any;
    findMany();
    save (agragate: any): string;
    update();
    remove();
}